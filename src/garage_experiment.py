#!/usr/bin/env python

import datetime
import os
from pathlib import Path
import sys
import yaml

import enoslib as en

from garage_helpers import Garage

if len(sys.argv) < 2:
    print(f"usage: {sys.argv[0]} <input/params.yaml>")
    exit(1)

en.check()

_ = en.init_logging()

# Parse parameters
params_file = sys.argv[1]
with open(params_file) as f:
    input_params = yaml.safe_load(f)
    # Keep a copy for reference (copied to output dir)
    f.seek(0)
    params_file_content = f.read()

# Used for G5K job name
params_file_name = params_file.split('/')[-1]

# Setup output dir
now = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d_%H-%M-%S")
output_dir = Path(f"output/{now}")
os.makedirs(output_dir, exist_ok=True)

print(f"Output dir for experiment: {output_dir}")

# Copy input parameters to output dir to keep it as reference
with open(output_dir / "input.yaml", "w") as f:
    f.write(params_file_content)

# Parameter sweeping
for i, params in enumerate(input_params["experiments"]):
    # Use output subdirectory
    local_output_dir = output_dir / params["name"]

    # Setup G5K job
    job_name = f"garage_{params_file_name}_{i}"
    conf = en.G5kConf().from_settings(
        job_type=[],
        job_name=job_name,
        walltime=input_params["g5k_walltime"],
    )

    # Add machines from parameters
    for machine in params["g5k_nodes"]:
        conf = conf.add_machine(**machine)

    conf.finalize()

    provider = en.G5k(conf)

    # Reserve resources
    roles, networks = provider.init()
    roles = en.sync_info(roles, networks)

    # Set custom host variables based on input.
    for host in roles.all():
        host.set_extra(**params["vars"])

    # Deploy Garage
    garage = Garage(garage_version=input_params["garage_version"],
                    nodes=roles["garage"])
    garage.deploy()

    # Run benchmark
    garage.benchmark(benchmark_node=roles["client"][0],
                     target_node=roles["target"][0],
                     output_dir=local_output_dir)

    # Release hardware resources
    provider.destroy()

print(f"Experiment finished. Output dir: {output_dir}")
