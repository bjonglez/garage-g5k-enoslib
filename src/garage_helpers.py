import datetime
import os
from pathlib import Path
import time
from typing import Dict, List, Union

import enoslib as en
from enoslib.api import run_ansible
from enoslib.objects import Host, Roles
from enoslib.service.service import Service


class Garage(Service):
    """Install and configure Garage on the given set of nodes, and
    setup all nodes to form a common cluster.

    Parameters:

      garage_version: which version to install

      nodes_config: a list of which nodes to deploy Garage on

    Attributes:

      self.rpc_secret: auto-generated Garage RPC secret

      self.admin_token: auto-generated token for the REST admin API [1]

      self.api_key: S3 API key

      self.bucket: S3 bucket

    [1] https://garagehq.deuxfleurs.fr/documentation/reference-manual/admin-api/

    """

    def __init__(self, garage_version: str, nodes: List[Host]):
        self.garage_version = garage_version
        self.nodes = nodes
        self.rpc_secret = os.urandom(32).hex()
        self.admin_token = os.urandom(32).hex()
        self.rust_target = "x86_64-unknown-linux-musl"
    
    def deploy(self):
        # TODO: improve template handling, and reuse
        # https://galaxy.ansible.com/ui/standalone/roles/zorun/garage/
        garage_config = f"""
metadata_dir = "/mnt/{{{{ garage_metadata }}}}/meta"
data_dir = "/mnt/{{{{ garage_data }}}}/data"

replication_mode = "3"
consistency_mode = "consistent"

rpc_bind_addr = "[::]:3901"
rpc_public_addr = "{{{{ ansible_default_ipv4.address }}}}:3901"
rpc_secret = "{self.rpc_secret}"

bootstrap_peers = []

[s3_api]
s3_region = "garage"
api_bind_addr = "[::]:3900"
root_domain = ".s3.garage.localhost"

[s3_web]
bind_addr = "[::]:3902"
root_domain = ".web.garage.localhost"
index = "index.html"

[admin]
api_bind_addr = "[::]:3903"
admin_token = "{self.admin_token}"
"""
        import tempfile
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as tmp:
            config_template = tmp.name
            print(garage_config, file=tmp)

        # Setup mountpoints to respect garage_data and garage_metadata.
        # We mount the filesystem to a predictable location: /mnt/memory/, /mnt/disk0, /mnt/disk1, ...
        # The case of disk0 is special since it's already used by the OS, so we do a bind-mount.
        # For diskN with N>0:
        # - we format a ext4 filesystem directly on the disk (no partition)
        # - we disable lazy ext4 init because it generates background I/O write during the benchmark
        # - since mkfs can take ages in that case (10 minutes for a 4 TB HDD), we artifically limit the filesystem size to speed things up
        fs_size = "100G"
        with en.actions(roles=self.nodes, gather_facts=False) as p:
            p.shell(
                "mkdir -p /dev/shm/garage /mnt/memory; umount /mnt/memory; mount --bind /dev/shm/garage /mnt/memory",
                when="garage_data == 'memory' or garage_metadata == 'memory'",
                task_name="Setup mountpoint for 'memory' storage",
            )
            p.shell(
                "mkdir -p /tmp/garage_storage /mnt/disk0; umount /mnt/disk0; mount --bind /tmp/garage_storage /mnt/disk0",
                when="garage_data == 'disk0' or garage_metadata == 'disk0'",
                task_name="Setup mountpoint for 'disk0' storage",
            )
            p.shell(
                "mkdir -p /mnt/{{garage_data}}; "
                "umount /mnt/{{garage_data}}; "
                "mkfs.ext4 -F -m 0 -E lazy_itable_init=0 /dev/{{garage_data}} " + fs_size + "; "
                "mount -o noatime /dev/{{garage_data}} /mnt/{{garage_data}}",
                when="garage_data != 'disk0' and garage_data | regex_search('disk[0-9]+')",
                task_name="Format and setup mountpoint for storage of data on 'diskN'",
            )
            # Don't reformat/remount if metadata is in the same location as data
            p.shell(
                "mkdir -p /mnt/{{garage_metadata}}; "
                "umount /mnt/{{garage_metadata}}; "
                "mkfs.ext4 -F -m 0 -E lazy_itable_init=0 /dev/{{garage_metadata}}" + fs_size + "; "
                "mount -o noatime /dev/{{garage_metadata}} /mnt/{{garage_metadata}}",
                when="garage_metadata != garage_data and garage_metadata != 'disk0' and garage_metadata | regex_search('disk[0-9]+')",
                task_name="Format and setup mountpoint for storage of metadata on 'diskN'",
            )
            results = p.results
        # Check that at least one task was applied for each host
        for host in self.nodes:
            if not any(results.filter(host=host.address, status="OK")):
                raise RuntimeError(f"Host {host.address} had no mountpoint setup, this is likely a bug in the input of the experiment")

        # Download Garage binary locally (on controller host) to avoid
        # wasteful parallel downloads when using many nodes
        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            p.get_url(
                url=f"https://garagehq.deuxfleurs.fr/_releases/v{self.garage_version}/{self.rust_target}/garage",
                dest=f"/tmp/garage-{self.garage_version}",
                mode="755",
                task_name="Downloading garage binary locally",
                delegate_to="localhost",
                run_once=True,
            )

        with en.actions(roles=self.nodes, gather_facts=True) as p:
            p.copy(
                src=f"/tmp/garage-{self.garage_version}",
                dest="/tmp/garage",
                mode="755",
                task_name="Downloading garage"
            )
            p.template(
                task_name="Creating config",
                src=config_template,
                dest="/tmp/garage.toml"
            )
            p.command(
                task_name="Kill garage if already running",
                cmd="killall garage",
                ignore_errors=True,
            )
            p.command(
                task_name="Run garage in the background",
                cmd="/tmp/garage -c /tmp/garage.toml server",
                asynch=3600 * 24 * 365,
                poll=0
            )
            p.command(
                task_name="Get node ID",
                cmd="/tmp/garage -c /tmp/garage.toml node id -q",
            )
            results = p.results

        self.nodes_id = {r.host: r.stdout for r in results.filter(task="Get node ID")}

        with en.actions(roles=self.nodes, gather_facts=False) as p:
            # TODO: pas besoin de la double boucle
            for remote_node, remote_id in self.nodes_id.items():
                p.command(
                    task_name=f"Connect to remote node {remote_node}",
                    cmd=f"/tmp/garage -c /tmp/garage.toml node connect {remote_id}",
                )

        # Configure cluster: only run on a single node
        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            # Iterate on all nodes, and put each node in its own zone
            for node in self.nodes:
                zone = node.address.split('.')[0]
                id = self.nodes_id[node.address][:16]
                # TODO: use the admin API to configure all zones at once
                p.command(
                    task_name=f"Add node {node.address} to layout",
                    cmd=f"/tmp/garage -c /tmp/garage.toml layout assign -c 1000000 -z {zone} {id}",
                )
            p.uri(
                task_name="Get current layout version",
                url="http://localhost:3903/v1/layout",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="GET",
            )
            results = p.results
    
        layout_version = results.filter(task="Get current layout version")[0].payload['json']['version']

        # Apply layout (run on a single node)
        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            p.uri(
                task_name="Apply next layout version",
                url="http://localhost:3903/v1/layout/apply",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="POST",
                body_format="json",
                body={"version": layout_version + 1},
                status_code=[200, 204],
            )
            results = p.results

        # Create bucket (run on a single node)
        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            p.uri(
                task_name="Create API key",
                url="http://localhost:3903/v1/key",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="POST",
                body_format="json",
                body={"name": "enoslib-key"}
            )
            results = p.results

        self.api_key = results[0].payload['json']

        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            p.uri(
                task_name="Allow API key to create buckets",
                url=f"http://localhost:3903/v1/key?id={self.api_key['accessKeyId']}",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="POST",
                body_format="json",
                body={
                    "name": "enoslib-key",
                    "deny": None,
                    "allow": {"createBucket": True}
                }
            )
            p.uri(
                task_name="Create bucket",
                url="http://localhost:3903/v1/bucket",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="POST",
                body_format="json",
                body={"globalAlias": "enoslib-bucket"},
                status_code=[200, 409],
            )
            p.uri(
                task_name="Get bucket",
                url="http://localhost:3903/v1/bucket?globalAlias=enoslib-bucket",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="GET",
            )
            results = p.results

        self.bucket = results.filter(task="Get bucket")[0].payload['json']

        with en.actions(roles=self.nodes[0], gather_facts=False) as p:
            p.uri(
                task_name="Allow API key on bucket",
                url="http://localhost:3903/v1/bucket/allow",
                headers={"Authorization": f"Bearer {self.admin_token}"},
                method="POST",
                body_format="json",
                body={"bucketId": self.bucket["id"],
                      "accessKeyId": self.api_key["accessKeyId"],
                      "permissions": {"read": True, "write": True, "owner": True}
                      },
            )

    def benchmark(self, benchmark_node: Host, target_node: Host, output_dir: Union[Path, str]):
        """Run a S3 latency benchmark from [benchmark_node] to
        [target_node].  The target node must be part of a the Garage
        cluster."""
        os.makedirs(output_dir, exist_ok=True)
        # Setup benchmarking nodes
        with en.actions(roles=benchmark_node, gather_facts=False) as p:
            p.apt(task_name="Install golang",
                  name="golang",
                  state="present")
            p.git(task_name="Clone s3lat repository",
                  repo="https://gitlab.inria.fr/bjonglez/s3lat",
                  dest="/tmp/s3lat")
            p.command("go install",
                      chdir="/tmp/s3lat",
                      task_name="Build s3lat")
        #print(self.bucket)
        #print(self.api_key)
        garage_config_env = {
            "AWS_ACCESS_KEY_ID": self.api_key['accessKeyId'],
            "AWS_SECRET_ACCESS_KEY": self.api_key['secretAccessKey'],
            "REGION": 'garage',
            "ENDPOINT": f"{target_node.address}:3900",
            "OBJ_SIZE": 1024*128,
        }
        # Run benchmark sequentially (we have a single benchmark node for
        # now but let's be general).
        for host in [benchmark_node]:
            result = en.run_command("./go/bin/s3lat",
                                    roles=host,
                                    environment=garage_config_env,
                                    task_name="s3lat")
            # Store result of benchmark locally
            src = host.address.split('.')[0]
            dest = target_node.address.split('.')[0]
            with open(output_dir / f"s3lat_{src}_to_{dest}.csv", "w") as f:
                f.write(result[0].stdout)
            time.sleep(5)

# This is just an example
#with en.actions(roles=roles["client"][0], gather_facts=False) as p:
#    p.command(
#        cmd=f"my_s3_client s3://{bucket['id']}/directory",
#        chdir="/tmp",
#        environment=garage_config_env,
#        task_name="Example command using Garage-specific env variables"
#    )

