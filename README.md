# Garage experiment on Grid'5000

This is a complete distributed system experiment to demonstrate the use
of [EnOSlib](https://discovery.gitlabpages.inria.fr/enoslib/index.html).

[EnOSlib](https://discovery.gitlabpages.inria.fr/enoslib/index.html) is used to orchestrate the experiment (reserve hardware resources, install software, run a benchmark)

Hardware resources are taken from the [SLICES-FR / Grid'5000](https://www.grid5000.fr/w/Grid5000:Home) platform (you need an account)

The experiment involves benchmarking [Garage](https://garagehq.deuxfleurs.fr/), an open-source distributed object storage service tailored for self-hosting.

## Setting up the environment

We recommend running this experiment directly on Grid'5000, on a frontend.

Either connect through SSH:

    ssh -J access.grid5000.fr grenoble

Or start a notebook on a frontend at <https://intranet.grid5000.fr/notebooks> and open a terminal.

Clone the repository:

    cd
    git clone https://gitlab.inria.fr/bjonglez/garage-g5k-enoslib
    cd garage-g5k-enoslib

Then create a python virtualenv:

    python3 -m venv venv-garage

Finally, install dependencies (including EnOSlib):

    pip install -r requirements.txt

## Parametrizing the experiment

See `input/example.yaml`.  It defines the "parameter sweeping" part of the experiment.
For it each run, it defines:

- which nodes should be configured to form a Garage cluster (the "garage" role) 
- which node should run the benchmark (the "client" role)
- which node of the Garage cluster should be benchmarked (the "target" role, only the first node with this role is considered)
- variables that control where Garage's data and metadata will be stored.
  Can take the special value `memory` to use a tmpfs, or `diskN` where `N` is the ID of the physical disk to use ([example documentation for parasilo](https://www.grid5000.fr/w/Rennes:Hardware#parasilo))

The [Grid'5000 Hardware page](https://www.grid5000.fr/w/Hardware) is the reference to understand
the hardware characteristics of each cluster.

## Running the experiment

Start the experiment:

    . venv-garage/bin/activate
    python src/garage_experiment.py input/example.yaml

It should generate the result of the benchmarks in `output/`.

Then run the `garage-analysis.ipynb` notebook to analyse the results
